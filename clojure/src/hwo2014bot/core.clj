(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [hwo2014bot.solver :as solver]
            [clojure.algo.generic.math-functions :as math])
  (:use hwo2014bot.solver
        [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:import [hwo2014bot.solver Solver LinearRelation Line Bend])
  (:gen-class))

;; --------------------------------- State
(definterface IState
  (car_on_track_p [])
  (set_car_on_track [])
  (set_car_out_track [])
  
  (get_velocity [])
  (get_throttle [])
  (get_angle [])
  (get_piece_index [])
  (get_in_piece_distance [])

  (actualp [])
  (clear_car_data [])
  (upd [velocity throttle angle piece-ind piece-pos]))

(deftype State [name
                ^:volatile-mutable car-on-track
                ^:volatile-mutable velocity
                ^:volatile-mutable angle
                ^:volatile-mutable throttle
                ^:volatile-mutable piece-ind
                ^:volatile-mutable piece-pos]
  IState
  (car-on-track-p [this] car-on-track)
  (set-car-on-track [this] (set! car-on-track true))
  (set-car-out-track [this] (set! car-on-track false))
  
  (get-velocity [this] velocity)
  (get-throttle [this] throttle)
  (get-angle [this] angle)
  (get-piece-index [this] piece-ind)
  (get-in-piece-distance [this] piece-pos)
  (actualp [this] (>= velocity 0))
  (clear-car-data [this]
    (.upd this 0 0 0 piece-ind piece-pos))
  (upd [this v thr a p ppos]
    (set! velocity v)
    (set! throttle thr) 
    (set! angle a)
    (set! piece-ind p)
    (set! piece-pos ppos)))
(defn new-State [] (State. "rls" true -1 0 0 -1 -1))


;;; Global data
(def state (new-State))
(def *velocity-quanto-step* 0.1)
;(def *solver* (new-Solver))


;; ---------------------------------

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
   (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        nil
        ))));(System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))


;;; ---------------------------------
(defn form-piece [msg]
  (let [length (:length msg)
        switch (= (:switch msg) "true")
        radius (:radius msg)
        angle (:angle msg)]
    (cond length (Line. length radius)
          radius (Bend. radius angle switch)
          :else nil)))

(defmulti handle-msg
  (fn [msg solver]
    (if msg
      (:msgType msg)
      nil)))

(defmethod handle-msg "gameInit" [msg solver]
  (let [pieces (:pieces (:track (:race (:data msg))))]
    (.set-pieces solver (map (fn [p] (form-piece p)) pieces))
    (handle-msg nil solver)))

(defmethod handle-msg "carPositions" [msg solver]
  (if (.car-on-track-p state)
    (let [mydata (first (:data msg))
          angle (:angle mydata)
          ppos (:piecePosition mydata)
          pi (:pieceIndex ppos)
          in-p-dist (:inPieceDistance ppos)
          in-one-piece? (= pi (.get-piece-index state)) 
          actual-v (- in-p-dist (.get-in-piece-distance state))
          ]
      ;(println (str "piece: " pi " position: " in-p-dist))
      (let [max-v (.velocity solver pi in-p-dist)
            throttle (cond (and max-v (< actual-v max-v))
                           1.0 ;(.throttle solver max-v actual-v)
                           (not max-v) 1.0
                           :else 0.0)]
        ;(println (str "max-v:" max-v " actual-v: " actual-v))
        (cond (< actual-v 0)
              (do (.upd state (.get-velocity state) (.get-throttle state)
                        angle pi in-p-dist)
                  (handle-msg nil solver))
              :else (do (.upd state actual-v throttle angle pi in-p-dist)
                        {:msgType "throttle" :data throttle}))))
    (handle-msg nil solver)))

(defmethod handle-msg "spawn" [msg solver]
  (when (= "rls" (:name (:data msg)))
    (.set-car-on-track state))
  (handle-msg nil solver))

(defmethod handle-msg "crash" [msg solver]
  (when (= (.name state) (:name (:data msg)))
    (.add-crash-car-angle (.get-stat solver)
                          (.get-angle state))
    (.add-experiment-nmk-v solver
                           (- (.get-velocity state) *velocity-quanto-step*)
                           (.get-piece-index state)
                           (.get-in-piece-distance state))
    (.clear-car-data state)
    (.set-car-out-track state))
  (handle-msg nil solver))

(defmethod handle-msg :default [msg solver]
  ;(println "handle-msg :default")
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (if msg
    (case (:msgType msg)
      "yourCar" (println "yourCar")
      "gameInit" (println "gameInit")
      "carPositions" nil; (println "carPosition")

      "join" (println "Joined")
      "gameStart" (println "Race started")
      "crash" (println "Someone crashed")
      "gameEnd" (println "Race ended")
      "error" (println (str "ERROR: " (:data msg)))
      :else (println (str "not matched msg: " msg)))
    (println (str "msg is nil"))))

(defn game-loop [channel solver]
  (let [msg (read-message channel)
        handled (handle-msg msg solver)]
    (send-message channel handled))
  (recur channel solver))

(defn -main[& [host port botname botkey]]
  (.clear-car-data state)
  (.set-car-on-track state)
  (let [channel (connect-client-channel host (Integer/parseInt port))
        solver (new-Solver)]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel solver)
    solver))

;; hmm...

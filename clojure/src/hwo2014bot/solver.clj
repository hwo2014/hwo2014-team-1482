(ns hwo2014bot.solver
  (:require [clojure.data.json :as json]
            [clojure.algo.generic.math-functions :as math])
  (:gen-class))

(definterface IPiece
  (curvRadius []))

;;; ---------------------------------
;;; Storage
(defn storage-save [name obj]
  (spit name (.serialize obj)))

(defn storage-load [name deserialize-fn]
  (deserialize-fn (slurp name)))

;;; ---------------------------------
;;; Pieces
(deftype Line [length switch-line-ability?]
  IPiece
  (curvRadius [this]
    0.0))

(deftype Bend [radius angle switch-line-ability?]
  IPiece
  (curvRadius [this]
    (/ angle radius)))

;;; ---------------------------------
;;; Relation
(definterface ILinearRelation
  (getK [])
  (setK [val])
  (setAlfa [val])
  (getB [])
  (setB [val])
  (getY [x]))

(deftype LinearRelation [^:volatile-mutable k
                         ^:volatile-mutable b]
  ILinearRelation
  (getK [this] k)
  (setK [this val] (set! k val))
  (setAlfa [this val]
    (let [k (math/tan val)]
      (.setK this k)))
  (getB [this] b)
  (setB [this val] (set! b val))
  (getY [this x]
    (+ (* k x) b)))

(definterface INmk
  (value [x])
  (upd [experiments])
  (to_readable_string [])
  (serialize [])
  (is_usable [])
  (print []))

(definterface INmkLinear
  (set_a [v])
  (set_b [v])
  (set_x_sum [v])
  (set_y_sum [v])
  (set_xy_sum [v])
  (set_xq_sum [v])
  (set_n [v])
  (save [path])
  (tryload [path]))

(deftype NmkLinear [^:volatile-mutable a
                    ^:volatile-mutable b
                    ^:volatile-mutable x-sum
                    ^:volatile-mutable y-sum
                    ^:volatile-mutable xy-sum
                    ^:volatile-mutable xq-sum
                    ^:volatile-mutable n]
  INmkLinear
  (set-n [this v] (set! n v))
  (set-x-sum [this v] (set! x-sum v))
  (set-y-sum [this v] (set! y-sum v))
  (set-xy-sum [this v] (set! xy-sum v))
  (set-xq-sum [this v] (set! xq-sum v))
  (set-a [this v] (set! a v))
  (set-b [this v] (set! b v))
  (save [this path] (storage-save path this))

  (tryload [this path]
    (try
      (storage-load path
                    (fn [str]
                      (let [j (json/read-str str :key-fn keyword)]
                        (NmkLinear.
                         (:a j) (:b j) (:x-sum j) (:y-sum j)
                         (:xy-sum j) (:xq-sum j) (:n j)))))
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        this)))
  
  INmk
  (is-usable [this] (>= n 2))

  (serialize [this]
    (str "{\"a\":" a
         " \"b\":" b
         " \"x-sum\":" x-sum
         " \"y-sum\":" y-sum
         " \"xy-sum\":" xy-sum
         " \"xq-sum\":" xq-sum
         " \"n\":" n
         "}"))
  
  (to-readable-string [this]
    (str "a:" a " b:" b " n:" n " x-sum:" x-sum " y-sum:" y-sum
         " xy-sum:" xy-sum " xq-sum:" xq-sum))
  (print [this]
    (println (.to-readable-string this))
    this)

  (value [this x]
    (.getY (LinearRelation. a b) x))

  (upd [this experiments-yx]
    (let [[sy sx sxy sxq]
          (reduce (fn [acc ab]
                    (let [[sa sb sab sbq] acc
                          [a b] ab]
                      [(+ sa a)
                       (+ sb b)
                       (+ sab (* a b))
                       (+ sbq (* b b))]))
                  [0 0 0 0]
                  experiments-yx)]
      (.set-n this (+ n (count experiments-yx)))
      (.set-x-sum this (+ x-sum sx))
      (.set-y-sum this (+ y-sum sy))
      (.set-xy-sum this (+ xy-sum sxy))
      (.set-xq-sum this (+ xq-sum sxq))
      (when (> n 1)
        (let [na (/ (- (* n xy-sum) (* x-sum y-sum))
                  (- (* n xq-sum) (* x-sum x-sum)))
              nb (/ (- y-sum (* na x-sum)) n)]
          (.set-a this na)
        (.set-b this nb))))
    this))

(defn new-NmkLinear [] (NmkLinear. 0 0 0 0 0 0 0))

(defn deserialize-NmkLinear [str]
  (let [j (json/read-str str :key-fn keyword)]
      (NmkLinear.
       (:a j) (:b j) (:x-sum j) (:y-sum j) (:xy-sum j) (:xq-sum j) (:n j))))


;;; --------------------------------- Statistics
(definterface IStatistics
  (get_crash_car_angles [])
  (add_crash_car_angle [angle]))

(deftype Statistics [^:volatile-mutable crash-car-angles]
  IStatistics
  (get-crash-car-angles [this] crash-car-angles)
  (add-crash-car-angle [this angle]
    (println (str "add-crash-angle: angle: " angle))
    (set! crash-car-angles (cons angle crash-car-angles))))

(defn new-Statistics []
  (Statistics. []))

;;; ---------------------------------
;;; Solver
(definterface ISolver
  (get_pieces [])
  (set_pieces [pieces])
;  (get_velocity [piece-index in-piece-distance])
 ; (get_velocity [])
  (add_experiment_nmk_v [v piece-index in-piece-distance])
  (add_experiment_nmk_dv [v1 v2 throttle])
  (save_data [])
  (velocity [pi ipd])
  (throttle [v1 v2])
  (get_stat []))

(deftype Solver [^:volatile-mutable pieces 
                 nmk-v
                 nmk-dv
                 stat]
  ISolver
  (get-stat [this] stat)
  (get-pieces [this] pieces)
  (set-pieces [this new-pieces]
    (set! pieces
          ;          (map-indexed (fn [i piece] [i piece])
                       new-pieces))
  (velocity [this piece-index in-piece-distance]
    (cond (.is-usable nmk-v) (let [piece (nth pieces piece-index)]
                               (.value nmk-v
                                       (.curvRadius piece)))
          :else nil))
  (throttle [this v1 v2]
    (cond (.is-usable nmk-dv) (.value nmk-dv
                                      (math/abs (- v1 v2)))
          :else 1.0))
  (add-experiment-nmk-v [this v piece-index in-piece-distance]
    (println (str "add-experiment-nmk-v: v:" v " piece-index:" piece-index
                  " in-piece-distance:" in-piece-distance))
    (let [piece (nth pieces piece-index)
          cr (.curvRadius piece)]
      (.upd nmk-v [[v cr]])))
  (add-experiment-nmk-dv [this v1 v2 throttle]
    (println (str "add-experiment-nmk-dv: v1:" v1 " v2:" v2 " throttle:" throttle))
    (.upd nmk-dv [[(math/abs (- v1 v2)) throttle]]))
  (save-data [this]
    (.save nmk-v "nmk-v.json")
    (.save nmk-dv "nmk-dv.json")))

(defn new-Solver []
  (let [solv (Solver. []
                      (.tryload (new-NmkLinear) "nmk-v.json")
                      (.tryload (new-NmkLinear) "nmk-dv.json")
                      (new-Statistics))]
    solv))
    
